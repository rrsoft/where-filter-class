<?php
/**
 * Created by PhpStorm.
 * User: roza
 * Date: 09/06/2018
 * Time: 13:24
 */

class Search
{
    /**
     * @var array
     */
    private $_searches;

    /**
     * @var array
     */
    private $_wheres = array();

    /**
     * Promenná POST...
     * @var array
     */
    private $_post = array();

    /**
     * alias sql...
     * @var string
     */
    public $alias = null;

    /**
     * Do konstruktoru se dava jako prvni parametr polozky co te zajimaji
     * a jako druhy parametr post. To se samorejme da cele uchopit jinak.
     *
     * @param $searches array
     * @param $post array
     */
    public function __construct($searches, $post)
    {
        $this->_searches = $searches;
        $this->_post = $post;
    }

    /**
     * Setter pro generovani WHERE hodnot...
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->_wheres[$name] = $value;
    }

    /**
     * Prida sestaveny WHERE statement do pole mezi ostatni...
     * @param $value string
     */
    private function addToArray($value){
        array_push($this->_wheres, $value);
    }

    public function addLikePrefix($value, $wrap = "%"){
        return $wrap . $value;

    }

    public function addLikeSurfix($value){
        return $value . '%';
    }

    private function _getParsedSubQuery($subsearch, $count){
        return (($count) ? " OR " : "") . "(" . $subsearch->getItemName($this->alias) . $subsearch->operand . "'" . $this->addLikePrefix($this->_post[$subsearch->name], $subsearch->wrap) . "'" . ")";
    }

    public function setAlias($alias){
        $this->alias = $alias; return $this;
    }

    public function createQuery(){
        return $this->_parse();
    }

    /**
     * Tady se vytvari SQL statement podle predanych argumentu...
     * @return Search
     */
    private function _parse(){
        $_sub = array();
        foreach ($this->_searches as $search) {

            if($search->isSubQuery() === true){
                foreach ($search->name as $subsearch){
                    if($this->_post[$subsearch->name] !== null) {
                        $_sub[] = $this->_getParsedSubQuery($subsearch, count($_sub));
                    }
                }
                $sub_where = (count($_sub)) ? " AND (" . (implode("", $_sub)) . ")" : "";
                $this->addToArray((sizeof($this->_wheres)) ? "" . $sub_where : $sub_where);
            } else {

                if (!empty($this->_post[$search->name])) {
                    $_where = "(" . $search->getItemName($this->alias) . $search->operand . "'" . $this->_post[$search->name] . "'" . ")";
                    $this->addToArray((sizeof($this->_wheres)) ? " AND " . $_where : $_where);
                }
            }
        }
        return $this;
    }


    /**
     * Vrati hotovy SQL retezec, v zavislosti na predanem SQL.
     * Bud si nechas vratit jen WHER klasuli anebo si muzes nechat
     * vyparsovat cely SQL dotaz i ze selecteem.
     *
     * @param null $sql
     * @return string
     */
    public function getWhereStatement($sql = null){
        $tmp ='';
        foreach ($this->_wheres as $where){
            $tmp .= $where;
        }
        return ($sql) ? $sql . " " . $tmp : $tmp;
    }

}