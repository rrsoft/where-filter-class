<?php
/**
 * Created by PhpStorm.
 * User: roza
 * Date: 09/06/2018
 * Time: 13:43
 */

require_once "Search.php";
require_once "Item.php";

$firma      = new Item(array("name" => "firma", "operand"=> " = "));
$velikost   = new Item(array("name" => "velikost", "operand" => " > "));
$stav       = new Item(array("name" => "stav", "operand" => " NOT "));

$sublike1   = new Item(array("name" => "keywords", "operand" => " LIKE ", "wrap" => "%"));
$sublike2   = new Item(array("name" => "description", "operand" => " LIKE "));
$sublike3   = new Item(array("name" => "title", "operand" => " LIKE "));

$like       = new Item(array("name" => array($sublike1, $sublike2, $sublike3)));

$array = array($firma, $velikost, $stav, $like);

$_POST = array("firma" => "Eclair", "velikost" => 5, "stav" => true, "title" => 'tituek', "description" => 'poznámka');

$alias = 't1';
$sql = sprintf('SELECT %1$s.firma, %1$s.velikos, %1$s.stav FROM tabulka AS %1$s WHERE ', $alias);

$cls = new Search($array, $_POST);

$vysledny_dotaz = $cls->setAlias('t1')->createQuery()->getWhereStatement($sql) . " ORDER BY tab1.firma;";

var_dump($vysledny_dotaz);