<?php
/**
 * Created by PhpStorm.
 * User: roza
 * Date: 09/06/2018
 * Time: 13:54
 */

class Item
{
    /**
     * @var array
     */
    private $_item;

    /**
     * maly identifikator jestli je dotaz subquery
     * @var bool
     */
    private $_is_sub_query = false;


    public function isSubQuery(){
        return $this->_is_sub_query;
    }

    /**
     * pouze Item constructor.
     * @param $item
     */
    public function __construct($item)
    {
        $this->_item = $item;
        $this->_is_sub_query = is_array($item['name']);
    }

    /**
     * Getter na potrebne hodnoty...
     * @param $value
     * @return mixed
     */
    public function __get($value)
    {
        return $this->_item[$value];
    }

    public function getItemName($prefix = '')
    {
        if($this->isSubQuery()) {
            return null;
        }else{
            return ($prefix) ? $prefix . "." .$this->_item['name'] : $this->_item['name'];
        }
    }

}